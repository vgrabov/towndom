from django.views.generic.base import TemplateView 
from django.views.generic import ListView, DetailView
from models import Town, Sector, House, BaseObject
from django.shortcuts import get_object_or_404




class MyBaseView(TemplateView):
	menu_active = 0
	def get_context_data(self, *args, **kwargs):
		context = super(MyBaseView, self).get_context_data(*args, **kwargs)
		context['t_menu_active'] = self.menu_active
		return context




class HomePageView(MyBaseView):
    template_name = "home.html"
    menu_active = 1
    
    def get_context_data(self, **kwargs):
        context = super(HomePageView, self).get_context_data(**kwargs)
        context['town_list'] = Town.objects.filter(home_page=True)[:3]
        context['sector_list'] = Sector.objects.all()
       	context['house_list'] = House.objects.filter(home_page=True)[:3]
        return context



class OrderView(MyBaseView):
	menu_active = 4
	template_name = 'order.html'



class ContactView(MyBaseView):
	menu_active = 5
	template_name = 'contact.html'


class BaseObjectListView(ListView):
	paginate_by = 200
	menu_active = 0

	def get_context_data(self, *args, **kwargs):
		context = super(BaseObjectListView, self).get_context_data(*args, **kwargs)
		context['sector_list'] = Sector.objects.all()
		context['t_menu_active'] = self.menu_active
		return context




class TownList(BaseObjectListView, ListView):
	model = Town
	template_name = "base/town_list.html"
	menu_active = 2




class BaseObjectCartView(DetailView):
	template_name = 'base/object_cart.html'




class TownView(BaseObjectCartView):
	model = Town

	def get_queryset(self):
		return Town.objects.filter(slug=self.kwargs.get('slug', ''))




class HouseView(BaseObjectCartView):
	model = House

	def get_queryset(self, *args, **kwargs):
		return House.objects.filter(slug=self.kwargs.get('slug', ''))
	



class HouseList(BaseObjectListView):
	model = House
	template_name = "base/house_list.html"
	menu_active = 3




class FilterList(BaseObjectListView):
	template_name = "base/object_list.html"
	filter_context = {}


	def get_queryset(self, *args, **kwargs):
		request = self.request.GET.copy()
		town_list = Town.objects.all()
		house_list = House.objects.all()
		context = {}

		if request.get('sector'):
			context['sector'] = int(request.get('sector'))
			town_list = town_list.filter(sector=get_object_or_404(Sector, pk=request.get('sector')))
			house_list = house_list.filter(sector=get_object_or_404(Sector, pk=request.get('sector')))
		
		if request.get('min_price'):
			context['min_price'] = request.get('min_price')
			town_list = town_list.filter(price__gte=int(request.get('min_price')))
			house_list = house_list.filter(price__gte=int(request.get('min_price')))
		
		if request.get('max_price'):
			context['max_price'] = request.get('max_price')
			town_list = town_list.filter(price__lte=int(request.get('max_price')))
			house_list = house_list.filter(price__lte=int(request.get('max_price')))

		context['type'] = request.get('type')
		self.filter_context = context

		if request.get('type') == '0':
			return town_list
		if request.get('type') == '1':
			return house_list
		return list(town_list) + list(house_list)

	def get_context_data(self, *args, **kwargs):
		context = super(FilterList, self).get_context_data(*args, **kwargs)
		context['filter'] = self.filter_context
		return context