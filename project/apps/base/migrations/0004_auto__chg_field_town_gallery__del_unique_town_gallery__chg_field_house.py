# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):

    def forwards(self, orm):
        
        # Removing unique constraint on 'House', fields ['gallery']
        db.delete_unique('base_house', ['gallery_id'])

        # Removing unique constraint on 'Town', fields ['gallery']
        db.delete_unique('base_town', ['gallery_id'])

        # Changing field 'Town.gallery'
        db.alter_column('base_town', 'gallery_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['base.Gallery'], null=True))

        # Changing field 'House.gallery'
        db.alter_column('base_house', 'gallery_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['base.Gallery'], null=True))


    def backwards(self, orm):
        
        # User chose to not deal with backwards NULL issues for 'Town.gallery'
        raise RuntimeError("Cannot reverse this migration. 'Town.gallery' and its values cannot be restored.")

        # Adding unique constraint on 'Town', fields ['gallery']
        db.create_unique('base_town', ['gallery_id'])

        # User chose to not deal with backwards NULL issues for 'House.gallery'
        raise RuntimeError("Cannot reverse this migration. 'House.gallery' and its values cannot be restored.")

        # Adding unique constraint on 'House', fields ['gallery']
        db.create_unique('base_house', ['gallery_id'])


    models = {
        'base.gallery': {
            'Meta': {'object_name': 'Gallery'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'base.house': {
            'Meta': {'object_name': 'House'},
            'area': ('django.db.models.fields.FloatField', [], {}),
            'description': ('django.db.models.fields.TextField', [], {}),
            'gallery': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['base.Gallery']", 'null': 'True'}),
            'gas': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'home_page': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'inner_area': ('django.db.models.fields.FloatField', [], {}),
            'place': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'price': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'rate': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'sector': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['base.Sector']"}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50', 'db_index': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'wall': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'water': ('django.db.models.fields.PositiveIntegerField', [], {})
        },
        'base.picture': {
            'Meta': {'ordering': "['priority']", 'object_name': 'Picture'},
            'gallery': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['base.Gallery']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'picture': ('django.db.models.fields.files.FileField', [], {'max_length': '250'}),
            'priority': ('django.db.models.fields.PositiveIntegerField', [], {'default': '1'})
        },
        'base.sector': {
            'Meta': {'object_name': 'Sector'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        'base.town': {
            'Meta': {'object_name': 'Town'},
            'area': ('django.db.models.fields.FloatField', [], {}),
            'description': ('django.db.models.fields.TextField', [], {}),
            'entry': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'gallery': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['base.Gallery']", 'null': 'True'}),
            'gas': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'home_page': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'inner_area': ('django.db.models.fields.FloatField', [], {}),
            'place': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'price': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'rate': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'sector': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['base.Sector']"}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50', 'db_index': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'wall': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'water': ('django.db.models.fields.PositiveIntegerField', [], {})
        }
    }

    complete_apps = ['base']
