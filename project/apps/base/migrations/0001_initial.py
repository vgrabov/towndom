# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):

    def forwards(self, orm):
        
        # Adding model 'Sector'
        db.create_table('base_sector', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal('base', ['Sector'])

        # Adding model 'Town'
        db.create_table('base_town', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('slug', self.gf('django.db.models.fields.SlugField')(unique=True, max_length=50, db_index=True)),
            ('price', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('sector', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['base.Sector'])),
            ('place', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('description', self.gf('django.db.models.fields.TextField')()),
            ('gallery', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['base.Gallery'], unique=True)),
            ('inner_area', self.gf('django.db.models.fields.FloatField')()),
            ('area', self.gf('django.db.models.fields.FloatField')()),
            ('gas', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('water', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('wall', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('entry', self.gf('django.db.models.fields.PositiveIntegerField')()),
        ))
        db.send_create_signal('base', ['Town'])

        # Adding model 'House'
        db.create_table('base_house', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('slug', self.gf('django.db.models.fields.SlugField')(unique=True, max_length=50, db_index=True)),
            ('price', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('sector', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['base.Sector'])),
            ('place', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('description', self.gf('django.db.models.fields.TextField')()),
            ('gallery', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['base.Gallery'], unique=True)),
            ('inner_area', self.gf('django.db.models.fields.FloatField')()),
            ('area', self.gf('django.db.models.fields.FloatField')()),
            ('gas', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('water', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('wall', self.gf('django.db.models.fields.PositiveIntegerField')()),
        ))
        db.send_create_signal('base', ['House'])

        # Adding model 'Gallery'
        db.create_table('base_gallery', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
        ))
        db.send_create_signal('base', ['Gallery'])

        # Adding model 'Picture'
        db.create_table('base_picture', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('picture', self.gf('django.db.models.fields.files.FileField')(max_length=250)),
            ('priority', self.gf('django.db.models.fields.PositiveIntegerField')(default=1)),
            ('gallery', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['base.Gallery'])),
        ))
        db.send_create_signal('base', ['Picture'])


    def backwards(self, orm):
        
        # Deleting model 'Sector'
        db.delete_table('base_sector')

        # Deleting model 'Town'
        db.delete_table('base_town')

        # Deleting model 'House'
        db.delete_table('base_house')

        # Deleting model 'Gallery'
        db.delete_table('base_gallery')

        # Deleting model 'Picture'
        db.delete_table('base_picture')


    models = {
        'base.gallery': {
            'Meta': {'object_name': 'Gallery'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'base.house': {
            'Meta': {'object_name': 'House'},
            'area': ('django.db.models.fields.FloatField', [], {}),
            'description': ('django.db.models.fields.TextField', [], {}),
            'gallery': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['base.Gallery']", 'unique': 'True'}),
            'gas': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'inner_area': ('django.db.models.fields.FloatField', [], {}),
            'place': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'price': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'sector': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['base.Sector']"}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50', 'db_index': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'wall': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'water': ('django.db.models.fields.PositiveIntegerField', [], {})
        },
        'base.picture': {
            'Meta': {'ordering': "['priority']", 'object_name': 'Picture'},
            'gallery': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['base.Gallery']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'picture': ('django.db.models.fields.files.FileField', [], {'max_length': '250'}),
            'priority': ('django.db.models.fields.PositiveIntegerField', [], {'default': '1'})
        },
        'base.sector': {
            'Meta': {'object_name': 'Sector'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        'base.town': {
            'Meta': {'object_name': 'Town'},
            'area': ('django.db.models.fields.FloatField', [], {}),
            'description': ('django.db.models.fields.TextField', [], {}),
            'entry': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'gallery': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['base.Gallery']", 'unique': 'True'}),
            'gas': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'inner_area': ('django.db.models.fields.FloatField', [], {}),
            'place': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'price': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'sector': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['base.Sector']"}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50', 'db_index': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'wall': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'water': ('django.db.models.fields.PositiveIntegerField', [], {})
        }
    }

    complete_apps = ['base']
