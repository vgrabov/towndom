# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):

    def forwards(self, orm):
        
        # Adding field 'Town.rate'
        db.add_column('base_town', 'rate', self.gf('django.db.models.fields.PositiveIntegerField')(default=0), keep_default=False)

        # Adding field 'House.rate'
        db.add_column('base_house', 'rate', self.gf('django.db.models.fields.PositiveIntegerField')(default=0), keep_default=False)


    def backwards(self, orm):
        
        # Deleting field 'Town.rate'
        db.delete_column('base_town', 'rate')

        # Deleting field 'House.rate'
        db.delete_column('base_house', 'rate')


    models = {
        'base.gallery': {
            'Meta': {'object_name': 'Gallery'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'base.house': {
            'Meta': {'object_name': 'House'},
            'area': ('django.db.models.fields.FloatField', [], {}),
            'description': ('django.db.models.fields.TextField', [], {}),
            'gallery': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['base.Gallery']", 'unique': 'True'}),
            'gas': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'home_page': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'inner_area': ('django.db.models.fields.FloatField', [], {}),
            'place': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'price': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'rate': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'sector': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['base.Sector']"}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50', 'db_index': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'wall': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'water': ('django.db.models.fields.PositiveIntegerField', [], {})
        },
        'base.picture': {
            'Meta': {'ordering': "['priority']", 'object_name': 'Picture'},
            'gallery': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['base.Gallery']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'picture': ('django.db.models.fields.files.FileField', [], {'max_length': '250'}),
            'priority': ('django.db.models.fields.PositiveIntegerField', [], {'default': '1'})
        },
        'base.sector': {
            'Meta': {'object_name': 'Sector'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        'base.town': {
            'Meta': {'object_name': 'Town'},
            'area': ('django.db.models.fields.FloatField', [], {}),
            'description': ('django.db.models.fields.TextField', [], {}),
            'entry': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'gallery': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['base.Gallery']", 'unique': 'True'}),
            'gas': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'home_page': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'inner_area': ('django.db.models.fields.FloatField', [], {}),
            'place': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'price': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'rate': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'sector': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['base.Sector']"}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50', 'db_index': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'wall': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'water': ('django.db.models.fields.PositiveIntegerField', [], {})
        }
    }

    complete_apps = ['base']
