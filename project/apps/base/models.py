#-*- coding:utf-8 -*-
from django.db import models
from django.contrib import admin
from django.db.models.signals import post_save, pre_delete
from django.core.urlresolvers import reverse
from project.libs.utils import create_thumbnail, delete_thumbnail, get_thumbnail_url
from django.template import loader, Context
import os
# Create your models here.

GAS_TYPE = (
	(1, u'Есть'),
	(2, u'Нет'),
	(3, u'Можно подключить'),
)

WATER_TYPE = (
	(1, u'Скважина'),
	(2, u'Центральная'),
	(3, u'Нет')
)

WALL_TYPE = (
	(1, u'Кирпич'),
	(2, u'Пеноблок'),
)

ENTRY_TYPE = (
	(1, u'Индивидуальный'),
	(2, u'Общий')
)

class Sector(models.Model):
	title = models.CharField(max_length=255, verbose_name=u'район')

	def __unicode__(self):
		return self.title

	class Meta:
		verbose_name = u'район'
		verbose_name_plural = u'районы'




class BaseObject(models.Model):
	title = models.CharField(max_length=255, verbose_name=u'название')
	home_page = models.BooleanField(verbose_name=u"показывать на главной")
	slug = models.SlugField(verbose_name=u'системное имя', unique=True)
	price = models.PositiveIntegerField(verbose_name=u'цена')
	sector = models.ForeignKey('Sector', verbose_name=u'район')
	place = models.CharField(max_length=255, verbose_name=u'расположение')
	description = models.TextField(verbose_name=u'описание')
	gallery = models.ForeignKey('Gallery', verbose_name=u"галлерея", null=True, on_delete=models.SET_NULL)
	rate = models.PositiveIntegerField(verbose_name=u'рейтинг', default=0)

	def __unicode__(self):
		return self.title

	def get_announce(self, max_length=200):
		if len(self.description) < max_length:
			return self.description
		return self.description[:max_length] + "..."

	def get_url(self):
		return reverse(self.url_name, args=[self.slug,])

	class Meta:
		abstract = True




class BaseHouse(BaseObject):
	url_name = 'house_cart'
	inner_area = models.FloatField(verbose_name=u'площадь дома')
	area = models.FloatField(verbose_name=u'площадь участка')
	gas = models.PositiveIntegerField(choices=GAS_TYPE, verbose_name=u'газ')
	water = models.PositiveIntegerField(choices=WATER_TYPE, verbose_name=u'вода')
	wall = models.PositiveIntegerField(choices=WALL_TYPE, verbose_name=u'материал стен')

	def as_list(self):
		template = loader.get_template('base/snippets/as_list.html')
		context = Context({'object':self})
		return template.render(context)

	class Meta:
		abstract = True




class Town(BaseHouse):
	url_name = 'town_cart'
	entry = models.PositiveIntegerField(choices=ENTRY_TYPE, verbose_name=u'въезд')
	
	class Meta:
		verbose_name = u'таунхаус'
		verbose_name_plural = u'таунхаусы'




class House(BaseHouse):
	url_name = 'house_cart'

	class Meta:
		verbose_name = u'дом'
		verbose_name_plural = u'дома'




def update_filename(instance, filename):
    upload_dir = "pictures"
    return os.path.join(upload_dir, filename.lower())




class Gallery(models.Model):
	def get_main_pict(self):
		a=self.picture_set.all()
		if a:
			return a[0].picture

	class Meta:
		verbose_name = u"галлерея"
		verbose_name_plural = u"галлереи"





class Picture(models.Model):
    picture = models.ImageField(upload_to=update_filename, max_length=250)
    priority = models.PositiveIntegerField(verbose_name=u"приоритет", default=1)
    gallery = models.ForeignKey(Gallery)

    def get_url(self):
        return self.picture.url

    def get_thumb_url(self):
        return get_thumbnail_url(self.picture.url)

    def get_small_thumb_url(self):
        return get_thumbnail_url(self.picture.url, size=80)

    class Meta:
    	verbose_name = u"изображение"
    	verbose_name_plural = u"изображения"
    	ordering = ['priority',]
