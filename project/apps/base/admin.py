from django.contrib import admin
from .models import BaseObject, Town, House, Sector, Gallery, Picture
from sorl.thumbnail.admin import AdminImageMixin

class PictInline(admin.TabularInline, AdminImageMixin):
    model = Picture
    extra = 3

class GalleryAdmin(admin.ModelAdmin):
    inlines = [PictInline]
 

class PictAdmin(AdminImageMixin, admin.ModelAdmin):
	pass

admin.site.register(Town)
admin.site.register(House)
admin.site.register(Sector)
admin.site.register(Picture, PictAdmin)
admin.site.register(Gallery, GalleryAdmin)