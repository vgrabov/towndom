from django.conf.urls.defaults import *
from django.contrib import admin
from django.contrib.admin import site

from django.views.generic import TemplateView
from apps.base.views import *

admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', HomePageView.as_view(), name='home'),
    url(r'^list/town$', TownList.as_view(), name='towns'),
    url(r'^list/house$', HouseList.as_view(), name='houses'),
    url(r'^list/filter/', FilterList.as_view(), name='filter'),
    url(r'^town/(?P<slug>.+)$', TownView.as_view(), name='town_cart'),
    url(r'^house/(?P<slug>.+)$', HouseView.as_view(), name='house_cart'),
    url(r'^order$', OrderView.as_view(), name='order'),
    url(r'^contact$', ContactView.as_view(), name='contact'),


    
    # # enable the admin:
    url(r'^admin/', include(site.urls)),
)

# from django.conf import settings
# if settings.DEBUG:
#     urlpatterns += patterns(
#         '',
#         url(r'^media/(?P<path>.*)$', 'django.views.static.serve',
#                 {'document_root': settings.MEDIA_ROOT})
#     )
